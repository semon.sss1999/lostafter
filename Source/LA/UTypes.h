// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "UTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Walk_State UMETA(DisplayName = Walk State),
	Run_State UMETA(DisplayName = Run State),
	SprintRun_State UMETA(DisplayName = SprintRun State)

};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float WalkSpeedNormal = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float RunSpeedNormal = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float SprintSpeed = 600.0f;
};


UCLASS()
class LA_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};
